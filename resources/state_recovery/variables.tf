variable "region" {
  default     = "us-east-1"
  description = "Define your region"
}

variable "instance_count" {
  default = 4
}

variable "cluster_name" {
  default     = "default"
  description = "Defines you clusters name"
}

variable "disk_size" {
  default = 10
}

variable "vpc_id" {
  default = "vpc-7937641f"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "availability_zones" {
  default = {
    "0" = "eu-west-1a"
    "1" = "eu-west-1b"
    "2" = "eu-west-1c"
  }
}

variable "subnets_total" {
  default = "3"
}
