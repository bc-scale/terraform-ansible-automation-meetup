provider "aws" {
  region                  = "eu-east-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "marphio"
}
