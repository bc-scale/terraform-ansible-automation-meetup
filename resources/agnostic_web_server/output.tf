output "azure_public_ip" {
  value = "${module.azure_site_vm.public_ip}"
}

output "aws_public_ip" {
  value = "${module.aws_site_vm.public_ip}"
}

output "lb_public_ip" {
  value = "${module.aws_lb_vm.public_ip}"
}
