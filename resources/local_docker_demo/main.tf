provider "docker" {
  host = "${var.docker_host}"
}

resource "docker_image" "hello_world" {
  name = "crccheck/hello-world"
}

resource "docker_container" "hello_world" {
  count = "${var.container_count}"
  image = "${docker_image.hello_world.latest}"
  name  = "foo-${count.index}"

  ports {
    internal = 8000
  }
}
